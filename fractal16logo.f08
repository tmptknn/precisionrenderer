program fractalrenderer
  implicit none
  integer (kind=selected_int_kind(8)), parameter:: pixwidthparam = 4060
  integer (kind=selected_int_kind(8)), parameter:: pixheightparam = 2900
  integer (kind=selected_int_kind(8)), parameter:: numberofcolorsparam = 7
  logical, parameter:: readfractal = .TRUE.
  logical, parameter:: gencolors = .TRUE.
  logical, parameter:: rawgray = .FALSE.
  integer (kind=selected_int_kind(8)), dimension(:),allocatable:: my_pixbufin
  integer (kind=selected_int_kind(8)), dimension(:),allocatable:: my_pixbufout
  !integer, parameter :: qp = selected_real_kind(15, 307)
  integer, parameter :: qp = selected_real_kind(33, 4931)
  integer, parameter :: dp = selected_real_kind(15, 307)
  ! We use chars because we need unsigned integers:
  ! character(c_char), dimension(:), pointer :: pixel
  
  character(len=2):: signaturestring

  character(len=50):: numberstring;
  integer (kind=selected_int_kind(8)) :: nch, rowstride, pixval, pixwidth, pixheight, maxcolorcount
  ! integer(kind=selected_int_kind(2)) :: cstatus   ! Command status
  real (kind=dp), dimension(:,:), allocatable :: colors
  integer (kind=selected_int_kind(8)) :: i, numberofcolors, k
  
  !, mandelbrot, cubicinterpolate, fractional_part
  ! We create a "pixbuffer" to store the pixels of the image.
  ! This pixbuffer has no Alpha channel (15% faster), only RGB.
  ! https://developer.gnome.org/gdk-pixbuf/stable/gdk-pixbuf-The-GdkPixbuf-Structure.html
  ! pixwidth  = 4060*2
  ! pixheight = 2900*2
  !my_pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 16_c_int, &
   !                        & pixwidth, pixheight)
  !nch = gdk_pixbuf_get_n_channels(my_pixbuf)

  !call c_f_pointer(gdk_pixbuf_get_pixels(my_pixbuf), pixel, &
  !               & (/pixwidth*pixheight*nch/))

  ! The background is black (red=0, green=0, blue=0):
  !pixel = char(0)

  if(readfractal) then
    call readimage(my_pixbufin, 'logograytp7.pam', pixwidth, pixheight, nch, rowstride)
    allocate(my_pixbufout(pixheight*pixwidth*nch))
  else
    pixwidth = pixwidthparam
    pixheight = pixheightparam
    nch = 4
    rowstride = pixwidth*nch
    allocate(my_pixbufin(pixheight*pixwidth*nch))
    allocate(my_pixbufout(pixheight*pixwidth*nch))
    !rowstride = gdk_pixbuf_get_rowstride(my_pixbuf)
    print *, "Channels= ", nch, "      Rowstride=", rowstride
  endif

  if (gencolors) then
    numberofcolors = numberofcolorsparam
    allocate(colors(numberofcolors,3))
  end if

  !do k = 0, 200
  
    write(numberstring, '(i4.4)') k
    if (gencolors) then
        do i = 1, numberofcolors
            call random_number(colors(i,1))
            call random_number(colors(i,2))
            call random_number(colors(i,3))
        end do
        ! call savepalette('fouroutpalette'//trim(adjustl(numberstring))//'.bin', colors, numberofcolors)
        call savepalette('logopalette2.bin', colors, numberofcolors)
    else
        call loadpalette('logopalette2.bin', colors, numberofcolors)
    end if

    call renderimage(my_pixbufin, my_pixbufout, pixwidth, pixheight, nch, rowstride, colors, numberofcolors)
    print *, 'Done rendering'
    !!call saveimage(my_pixbufout, 'fourout'//trim(adjustl(numberstring))//'.ppm', pixwidth, pixheight, nch, rowstride)
    call saveimage(my_pixbufout, 'logocolortp7.pam', pixwidth, pixheight, nch, rowstride)
    print *, 'Done saving'
  !end do
  print *, 'Completed'

  contains

    subroutine renderimage(my_pixbufin, my_pixbufout, pixwidth,pixheight, nch, rowstride, colors, numberofcolors)
        implicit none
        integer (kind=selected_int_kind(8)), intent(in), dimension(:),allocatable:: my_pixbufin
        integer (kind=selected_int_kind(8)), intent(inout), dimension(:),allocatable:: my_pixbufout
        integer (kind=selected_int_kind(8)), intent(in) :: nch, rowstride, pixwidth, pixheight
        integer (kind=selected_int_kind(8)) :: i, j
        real (kind=qp) :: r, jj, ii, fwidth, fheight,rr,rg,rb,ra,t
        real (kind=dp) :: cmu
        integer (kind=selected_int_kind(8)) :: p,pa, ri, c0, c1,c2,c3
        integer (kind=selected_int_kind(8)) :: n, x, y
        real (kind=qp) :: rsum, gsum, bsum, asum, step 
        real (kind=dp), dimension(:,:), intent(in),allocatable:: colors
        integer (kind=selected_int_kind(8)), intent(in) :: numberofcolors

        do concurrent (j = 0:pixheight-1)
            print *, real(j)*100.0/real(pixheight)
            do i = 0, pixwidth-1
                t=real(0.0, qp)
                ii = real(i, qp)
                jj = real(j, qp)
                fwidth = real(pixwidth, qp)
                fheight = real(pixheight, qp)
                rsum = real(0.0, qp)
                gsum = real(0.0, qp)
                bsum = real(0.0, qp)
                asum = real(0.0, qp)
                n = 5;
                step = real(1.0,qp)/real(n,qp)
                if(.NOT. readfractal) then
                    do x =-2, 2
                        do y=-2, 2
                            r = mandelbrot(ii+x*step,jj+y*step,fwidth,fheight)
                            if (.NOT. rawgray) then
                                r=r*real(0.02, qp)
                                ri = floor(r)
                                cmu = real(fractional_part(r), dp)
                                c0 = mod(ri,numberofcolors)+1
                                c1 = mod(ri+1,numberofcolors)+1
                                c2 = mod(ri+2,numberofcolors)+1
                                c3 = mod(ri+3,numberofcolors)+1
                                rr = cubicInterpolate(colors(c0,1),colors(c1,1),colors(c2,1),colors(c3,1),cmu)
                                rg = cubicInterpolate(colors(c0,2),colors(c1,2),colors(c2,2),colors(c3,2),cmu)
                                rb = cubicInterpolate(colors(c0,3),colors(c1,3),colors(c2,3),colors(c3,3),cmu)
                            else
                                rr = r
                                rg = r
                                rb = r
                                if(r .eq. real(0.0, qp)) then
                                ra = real(0.0, qp)
                                else
                                ra = real(1.0, qp)
                                end if
                            end if
        
                            if((x .eq. 0) .and. (y .eq. 0)) then
                                rr = rr*5
                                rg = rg*5
                                rb = rb*5
                                ra = ra*5
                            end if
        
                            if (r==0) then
                                rr = 0.0
                                rg = 0.0
                                rb = 0.0
                                ra = 0.0
                            end if
                            rsum = rsum + rr
                            gsum = gsum + rg
                            bsum = bsum + rb
                            asum = asum + ra
                        end do
                    end do
                
                    rr = rsum/(n*n+4)
                    rg = gsum/(n*n+4)
                    rb = bsum/(n*n+4)
                    ra = asum/(n*n+4)
        
                    rr = max(rr,real(0.0,qp))
                    rg = max(rg,real(0.0,qp))
                    rb = max(rb,real(0.0,qp))
                    ra = max(ra,real(0.0,qp))
                    rr = min(rr,real(1.0,qp))
                    rg = min(rg,real(1.0,qp))
                    rb = min(rb,real(1.0,qp))
                    ra = min(ra,real(1.0,qp))
                else
                    p = 1 + i*nch + j*rowstride
                    r = real(my_pixbufin(p),qp)/real(65535,qp)
                    pa = 4 + i*nch + j*rowstride
                    ra = real(my_pixbufin(pa),qp)/real(65535,qp)
                    if( ra>real(0.0,qp)) then
                        ra=1.0
                    endif
                    r=r*real(512,qp)
        
                    r=r*real(0.02, qp)
                    ri = floor(r)
                    cmu = real(fractional_part(r), dp)
                    c0 = mod(ri,numberofcolors)+1
                    c1 = mod(ri+1,numberofcolors)+1
                    c2 = mod(ri+2,numberofcolors)+1
                    c3 = mod(ri+3,numberofcolors)+1
                    rr = cubicInterpolate(colors(c0,1),colors(c1,1),colors(c2,1),colors(c3,1),cmu)
                    rg = cubicInterpolate(colors(c0,2),colors(c1,2),colors(c2,2),colors(c3,2),cmu)
                    rb = cubicInterpolate(colors(c0,3),colors(c1,3),colors(c2,3),colors(c3,3),cmu)
        
        
                    rr = max(rr,real(0.0,qp))
                    rg = max(rg,real(0.0,qp))
                    rb = max(rb,real(0.0,qp))
                    rr = min(rr,real(1.0,qp))
                    rg = min(rg,real(1.0,qp))
                    rb = min(rb,real(1.0,qp))
                    
                    if (r==0) then
                        rr = 0.0;
                        rg = 0.0;
                        rb = 0.0;
                        ra = 0.0;
                    end if
                end if
                ! Position of the corresponding pixel in the pixbuffer:
                p = 1 + i*nch + j*rowstride
                ! Red, Green, Blue values computed from the distances to vertices:
                my_pixbufout(p)   = min(int(65535 * rr),65535)
                my_pixbufout(p+1) = min(int(65535 * rg),65535)
                my_pixbufout(p+2) = min(int(65535 * rb),65535)
                my_pixbufout(p+3) = min(int(65535 * ra),65535)
            end do
        end do
    endsubroutine renderimage

    subroutine loadpalette(filename, colors, numberofcolors)
        implicit none
        real (kind=dp), dimension(:,:), intent(inout),allocatable:: colors
        integer (kind=selected_int_kind(8)), intent(inout) :: numberofcolors
        integer (kind=selected_int_kind(8)) ::i, filehandle
        character(len=*), intent(in) :: filename
        filehandle = 12
        open(filehandle, file = filename, status='old', access='stream') 
        read(filehandle) numberofcolors
        allocate(colors(numberofcolors,3))
        do i=1, numberofcolors
          read(filehandle) colors(i,1)
          read(filehandle) colors(i,2)
          read(filehandle) colors(i,3)
        end do
        close(filehandle)
    end subroutine loadpalette

    subroutine savepalette(filename, colors,numberofcolors)
        implicit none
        real (kind=dp), dimension(:,:), intent(in),allocatable:: colors
        integer (kind=selected_int_kind(8)), intent(in) :: numberofcolors
        integer (kind=selected_int_kind(8)) ::i, filehandle
        character(len=*), intent(in) :: filename
        filehandle = 12
        if (gencolors) then
            print *, 'Saving palette'
            open(filehandle, file = filename, status = 'new', access='stream') 
            write(filehandle) numberofcolors
            do i=1, numberofcolors
                write(filehandle) colors(i,1)
                write(filehandle) colors(i,2)
                write(filehandle) colors(i,3)
            end do
            close(filehandle)
            print *, 'Palette saved'
          end if
    end subroutine savepalette

    subroutine saveimage(my_pixbuf, filename, pixwidth,pixheight, nch, rowstride)
        implicit none
        integer (kind=selected_int_kind(8)), intent(in), dimension(:),allocatable:: my_pixbuf
        integer (kind=selected_int_kind(8)), intent(in) :: nch, rowstride, pixwidth, pixheight
        integer (kind=selected_int_kind(8)) :: i, j, filehandle
        character(len=50):: widthstring, heightstring
        character(len=*), intent(in) :: filename
        filehandle = 11
        
        write(widthstring, *) pixwidth
        write(heightstring, *) pixheight
        
        open(filehandle, file = filename, status = 'new',access='stream') 
        write(filehandle) "P7"
        write(filehandle) new_line('a')
        write(filehandle) "WIDTH "
        write(filehandle) trim(adjustl(widthstring))
        write(filehandle) new_line('a')
        !write(filehandle) " "
        write(filehandle) "HEIGHT "
        write(filehandle) trim(adjustl(heightstring))
        write(filehandle) new_line('a')

        write(filehandle) "DEPTH 4"
        write(filehandle) new_line('a')
        !write(filehandle) " "
        write(filehandle) "MAXVAL 65535"
        write(filehandle) new_line('a')
        write(filehandle) "TUPLTYPE RGB_ALPHA"
        write(filehandle) new_line('a')
        write(filehandle) "ENDHDR"
        write(filehandle) new_line('a')
        do j = 0, pixheight-1
          print *, real(j)*100.0/real(pixheight)
          do i = 0, pixwidth-1
              pixval = my_pixbuf(1+i*nch+j*rowstride)
              write(filehandle) char((pixval-mod(pixval,256))/256)
              write(filehandle) char(mod(pixval,256))
              pixval = my_pixbuf(2+i*nch+j*rowstride)
              write(filehandle) char((pixval-mod(pixval,256))/256)
              write(filehandle) char(mod(pixval,256))
              pixval = my_pixbuf(3+i*nch+j*rowstride)
              write(filehandle) char((pixval-mod(pixval,256))/256)
              write(filehandle) char(mod(pixval,256))
              pixval = my_pixbuf(4+i*nch+j*rowstride)
              write(filehandle) char((pixval-mod(pixval,256))/256)
              write(filehandle) char(mod(pixval,256))
          end do
        end do  
        close(filehandle) 
    end subroutine saveimage

    subroutine readimage(my_pixbuf, filename, pixwidth, pixheight, nch, rowstride)
        implicit none
        integer (kind=selected_int_kind(8)), intent(inout), dimension(:),allocatable:: my_pixbuf
        integer (kind=selected_int_kind(8)), intent(inout) :: nch, rowstride, pixwidth, pixheight
        integer (kind=selected_int_kind(8)) ::offset, i, j, filehandle, depth
        character :: lowbyte,highbyte, ccode
        character(len=*), intent(in) :: filename
        character(len=100):: line
        integer :: io_stat
        filehandle = 11
        open(filehandle, file = filename, status = 'old', form='formatted',access='stream')         
        read(filehandle, '(a2)') signaturestring
        if(signaturestring /= "P7") then
            close(filehandle)
            write(*,*) 'Wrong type image file'
            stop
        end if

        read(filehandle, *) line, pixwidth
        print *, "pixwidth",pixwidth
        nch = 4
        rowstride = pixwidth*nch
        !rowstride = gdk_pixbuf_get_rowstride(my_pixbuf)
        print *, "Channels= ", nch, "      Rowstride=", rowstride

        read(filehandle,*) line, pixheight
        print *, "pixheight",pixheight

        read(filehandle,*) line, depth

        print *, "depth",depth



        read(filehandle,*) line, maxcolorcount
        if(maxcolorcount /= 65535) then
            close(filehandle)
            write(*,*) 'wrong number of colors'
            stop
        end if

        read(filehandle, *) line


        read(filehandle, *) line

        print *, line

        allocate(my_pixbuf(pixheight*pixwidth*nch))
        inquire(filehandle, pos=offset)
        close(filehandle)

        open(filehandle, file = filename, status = 'old',access='stream')
        read(filehandle, pos=offset-1) ccode
        do j = 0, pixheight-1
            print *, real(j)*100.0/real(pixheight)
            do i = 0, pixwidth-1

                read(filehandle) highbyte
                read(filehandle) lowbyte
                my_pixbuf(1+i*nch+j*rowstride) = iachar(highbyte)*256+iachar(lowbyte)

                read(filehandle) highbyte
                read(filehandle) lowbyte
                my_pixbuf(2+i*nch+j*rowstride) = iachar(highbyte)*256+iachar(lowbyte)

                read(filehandle) highbyte
                read(filehandle) lowbyte
                my_pixbuf(3+i*nch+j*rowstride) = iachar(highbyte)*256+iachar(lowbyte)

                
                read(filehandle) highbyte
                read(filehandle) lowbyte
                my_pixbuf(4+i*nch+j*rowstride) = iachar(highbyte)*256+iachar(lowbyte)

            end do
        end do  
        close(filehandle) 

    end subroutine readimage

    pure subroutine rotationmatrix(r,value)
        implicit none
        real (kind=qp), intent(in) :: r
        real (kind=qp), dimension(2,2), intent(out) :: value

        value(1,1) = cos(r)
        value(2,1) = -sin(r)
        value(1,2) = sin(r)
        value(2,2) = cos(r)
    end subroutine rotationmatrix

    pure real (kind=qp) function cubicinterpolate(y0, y1, y2, y3, mu)
        implicit none
        real (kind=dp), intent(in) :: y0,y1,y2,y3,mu
        real (kind=dp) :: a0,a1,a2,a3,mu2

        mu2 = mu * mu;
        a0 = y3 - y2 - y0 + y1;
        a1 = y0 - y1 - a0;
        a2 = y2 - y0;
        a3 = y1;
        cubicinterpolate = a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3;
    end function cubicinterpolate

    pure real (kind=qp) function fractional_part(x)
        implicit none
        real (kind=qp), intent(in) :: x
        fractional_part=x-floor(x)
    end function fractional_part

    pure real (kind=qp) function mandelbrot(i,j,width,height)
        implicit none
        integer(kind=selected_int_kind(4)) :: n, iterations
        real (kind=qp), intent(in) :: i,j, width, height
        real (kind=qp) :: cr, ci,aspectRatio, zoom, rot, lg2, csquare
        real (kind=qp), dimension(2,2) :: rotation, zoomMatrix
        real (kind=qp), dimension(2) :: offset, current,c,z
        ! complex (kind=qp) :: c, z
        lg2= log(real(2,qp))

        aspectRatio = width/height

        cr = -real(aspectRatio, qp)/real(2.0,qp)+real(i,qp)*aspectRatio/width
        ci = real(0.5, qp)-real(j,qp)/height

        !offset(1) = real(-0.7307999774585782, qp)
        !offset(2) = real(0.2406769623170887, qp)
        !zoom = real(0.0002261760232165877,qp)
        !rot = real(5.917862295953283, qp)

        ! offset(1) = real(-0.7703480417963366, qp)
        ! offset(2) = real(-0.10898065798566837, qp)
        ! zoom = real(0.0018985999539854353,qp)
        ! rot = real(3.4387344289628308, qp)
        
        ! offset(1) = real(-0.7336119162628065, qp)
        ! offset(2) = real(0.16840769052870844, qp)
        ! zoom = real(0.0015359359268444259,qp)
        ! rot = real(-54.282183634323694, qp)

        ! offset(1) = real(-1.1457037025841899, qp)
        ! offset(2) = real(-0.26655582753233725, qp)
        ! zoom = real(0.00015886254531880981,qp)
        ! rot = real(-35.265497529085366, qp)


        ! offset(1) = real(-0.75, qp)
        ! offset(2) = real(0, qp)
        ! zoom = real(2,qp)
        ! rot = real(0, qp)

        ! offset(1) = real(-1.1092585216813335, qp)
        ! offset(2) = real(-0.23634255125608702, qp)
        ! zoom = real(0.00008957028427187832,qp)
        ! rot = real(28.401720748072385, qp)

        !offset(1) = real(-0.8796003036991142, qp)
        !offset(2) = real(0.225613242521193, qp)
        !zoom = real(0.0024164874384466576,qp)
        !rot = real(63.89725980876932, qp)


        ! offset(1) = real(-0.7784590986270224, qp)
        ! offset(2) = real(0.1267887931396932, qp)
        ! zoom = real(0.001275260251262353,qp)
        ! rot = real(1.9234928842857468, qp)

        ! offset(1) = real(-0.6949709081794548, qp)
        ! offset(2) = real(0.2908838962677287, qp)
        ! zoom = real(0.0007314690918205771,qp)
        ! rot = real(13.324548803984491, qp)

        !offset(1) = real(-0.568923534433705, qp)
        !offset(2) = real(0.4823627213614685, qp)
        !zoom = real(0.0018873539745170756,qp)
        !rot = real(13.416389762477714, qp)

        ! offset(1) = real(-0.21629389117173053, qp)
        ! offset(2) = real(-0.7092033783615664, qp)
        ! zoom = real(0.0014794441937814708,qp)
        ! rot = real(20.403266687614874, qp)


        offset(1) = real(-0.26996483149770056, qp)
        offset(2) = real(-0.6319053949639674, qp)
        zoom = real(0.002745167257356244 ,qp)
        rot = real(0.6847166023220432, qp)


        zoomMatrix(1,1) = zoom
        zoomMatrix(1,2) = zoom
        zoomMatrix(2,1) = zoom
        zoomMatrix(2,2) = zoom
        call rotationmatrix(rot,rotation)
        
        ! cr = cr*zoom
        ! ci = ci*zoom

        current(1) = cr
        current(2) = ci

        current = matmul(rotation,current)
        current(1) = current(1)*zoom+offset(1)
        current(2) = current(2)*zoom+offset(2)

        c = current ! cmplx(current(1),current(2))

        iterations = 512
        z = c
        mandelbrot=0.0
        do n = 0, iterations
            ! z = z*z + c;
            cr = z(1) * z(1) - z(2) * z(2) + c(1)
            ci = real(2.0, qp) * z(1) * z(2) + c(2)
            z(1) = cr
            z(2) = ci
            csquare =z(1)*z(1)+z(2)*z(2)
            if ( csquare >real(8.0,qp)) then
                
                mandelbrot =  (real(1.0,qp) - log(log(csquare)/(lg2*real(8.0,qp)))/lg2 + real(n, qp)) ! /real(iterations, qp) 
                if (rawgray) mandelbrot = mandelbrot /real(iterations, qp) 
                return
            end if
        end do
        mandelbrot=0.0
        ! value = (ii*jj)/(fwidth*fheight)
    end function mandelbrot

    function supersample(i,j,width,height) result(value)
        implicit none
        integer(kind=selected_int_kind(4)) :: n, x, y
        real (kind=qp) :: value, i,j,width,height, step, sum, mandelbrot

        n=3;
        step = real(1.0,qp)/real(n,qp)
        sum = real(0.0, qp)

        do x =0, n
            do y=0, n
                sum = sum + mandelbrot(i+x*step,j+y*step,width,height)
            end do
        end do
        value = sum/(real(n,qp)*real(n,qp))
    end function supersample

    elemental subroutine str2int(str,int,stat)
        implicit none
        ! Arguments
        character(len=*),intent(in) :: str
        integer,intent(out)         :: int
        integer,intent(out)         :: stat

        read(str,*,iostat=stat)  int
    end subroutine str2int

    function ReadLine(aunit, InLine, trimmed) result(OK)
        integer, intent(IN) :: aunit
        character(LEN=:), allocatable, optional :: InLine
        logical, intent(in), optional :: trimmed
        integer, parameter :: line_buf_len= 1024*4
        character(LEN=line_buf_len) :: InS
        logical :: OK, set
        integer status, size
        
        OK = .false.
        set = .true.
        do
            read (aunit,'(a)',advance='NO',iostat=status, size=size) InS
            OK = .not. IS_IOSTAT_END(status)
            if (.not. OK) return
            if (present(InLine)) then
                if (set) then
                    InLine = InS(1:size)
                    set=.false.
                else
                    InLine = InLine // InS(1:size)
                end if
            end if
            if (IS_IOSTAT_EOR(status)) exit
        end do
        if (present(trimmed) .and. present(InLine)) then
            if (trimmed) InLine = trim(adjustl(InLine))
        end if
        
    end function ReadLine
end program fractalrenderer
